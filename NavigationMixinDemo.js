import { LightningElement } from 'lwc';
import { NavigationMixin } from 'lightning/navigation';

export default class NavigationMixinDemo extends NavigationMixin(LightningElement) {
    navigatetohome(){
        this[NavigationMixin.Navigate]({
            type: 'standard__objectPage',
            attributes: {
                objectApiName: 'Account',
                actionName: 'home',
            },
        });
    }
    navigatetorecord(){
        this[NavigationMixin.Navigate]({
            type: 'standard__recordPage',
            attributes: {
                recordId : '0015g00000RkZhWAAV',
                actionName: 'view',
            }
        });
    }
    navigatetotab(){
        this[NavigationMixin.Navigate]({
            type: 'standard__navItemPage',
            attributes: {
                apiName: 'LWCDemo1',
            }
        });
}
createnewrecord(){
    this[NavigationMixin.Navigate]({
        type: 'standard__objectPage',
        attributes: {
            objectApiName: 'Contact',
            actionName: 'new'
        },
    });
}

gotoexternalurl(){
    this[NavigationMixin.Navigate]({
        type: 'standard__webPage',
        attributes: {
           url : 'https://www.mtxb2b.com'
        },
    });
}

 
}